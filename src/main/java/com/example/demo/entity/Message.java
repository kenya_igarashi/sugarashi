package com.example.demo.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "messages")
public class Message {
	@Id
	@Column

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	@NotBlank(message = "タイトルを入力してください")
	@Size(max = 30, message = "30字以下で入力してください")
	private String title;

	@Column
	@NotBlank(message = "本文を入力してください")
	@Size(max = 1000, message = "1000字以下で入力してください")
	private String text;

	@Column
	@NotBlank(message = "カテゴリーを入力してください")
	@Size(max = 10, message = "10字以下で入力してください")
	private String category;

	@Column
	private Integer userId;

	@Column
	private Date createdDate;

	@Column
	private Date updatedDate;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

    public Date getCreatedDate() {
    	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
    	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }

}
