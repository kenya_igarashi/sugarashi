package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SugarashiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SugarashiApplication.class, args);
	}

}
