package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Department;
import com.example.demo.entity.User;
import com.example.demo.service.BranchesService;
import com.example.demo.service.CommentService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;
import com.example.demo.utils.CipherUtil;

@Controller
@SessionAttributes(types = User.class)

public class UserController {
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	UserService userService;
	@Autowired
	BranchesService branchesService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	HttpSession session;

	//ログイン画面表示
	@GetMapping("/login")
	public ModelAndView login(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		// 空のentityを準備
		User loginUser = new User();
		mav.addObject("formModel", loginUser);
		if (request.getSession().getAttribute("errorMessages") != null) {
			mav.addObject("errorMessages", request.getSession().getAttribute("errorMessages"));
			request.getSession().removeAttribute("errorMessages");
		}
		// 画面遷移先を指定
		mav.setViewName("/login");
		return mav;
	}

	//ログイン処理
	@PostMapping("/loginUser")
	public ModelAndView loginUser(@ModelAttribute("formModel") User loginUser) {
		ModelAndView mav = new ModelAndView();

		// ユーザーを情報をセレクト
		User user = (User) userService.loginUser(loginUser);
        List<String> errorMessages = new ArrayList<String>();

		if (user == null) {
            errorMessages.add("ログインに失敗しました");

            //パスワードを空欄で上書き
            String password = loginUser.getPassword();
            password = "";
            loginUser.setPassword(password);
			mav.addObject("errorMessages", errorMessages);
			//ユーザー名のみ再セット
			mav.addObject("formModel", loginUser);
			// 画面遷移先を指定
			mav.setViewName("/login");
			return mav;
		} else if (user.getIsStopped() == 1) {
            errorMessages.add("ログインに失敗しました");

            //パスワードを空欄で上書き
            String password = loginUser.getPassword();
            password = "";
            loginUser.setPassword(password);
			mav.addObject("errorMessages", errorMessages);
			//ユーザー名のみ再セット
			mav.addObject("formModel", loginUser);
			// 画面遷移先を指定
			mav.setViewName("/login");
			return mav;
		}
		session.setAttribute("loginUser", user);

		// トップへリダイレクト
		return new ModelAndView("redirect:/sugarashi");
	}

	@GetMapping("/logout")
	public ModelAndView logoutUser() {
		//セッション情報クリア
		session.invalidate();
		return new ModelAndView("redirect:/login");
	}

	// ユーザー管理情報
	@GetMapping("/userManagement")
	public ModelAndView managementUser() {
		ModelAndView mav = new ModelAndView();
		// ユーザー、支社、部署を全件取得
		List<User> userData = userService.findAllUser();
		List<Branch> branchesData = branchesService.findAllBranches();
		List<Department> departmentData = departmentService.findAllDepartment();
		// 画面遷移先を指定
		mav.setViewName("/userManagement");
		// データオブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("branches", branchesData);
		mav.addObject("departments", departmentData);
		return mav;
	}

	// ユーザー編集画面
	@GetMapping("/editUser/{id}")
	public ModelAndView editUser(@Validated @PathVariable String id) {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		if(!id.matches("\\d{1,}")) {
			errorMessages.add("不正なパラメータが入力されました");
		}
		if (errorMessages.size() != 0) {
			// ユーザー、支社、部署を全件取得
			List<User> userData = userService.findAllUser();
			List<Branch> branchesData = branchesService.findAllBranches();
			List<Department> departmentData = departmentService.findAllDepartment();
			// データオブジェクトを保管
			mav.addObject("users", userData);
			mav.addObject("branches", branchesData);
			mav.addObject("departments", departmentData);
			mav.addObject("errorMessages", errorMessages);
			// 画面遷移先を指定
			mav.setViewName("/userManagement");
			return mav;
		}
		// 編集するユーザーを取得
		User user = userService.oneUser(Integer.parseInt(id));
		if(user == null) {
			errorMessages.add("不正なパラメータが入力されました");
		}
		if (errorMessages.size() != 0) {
			// ユーザー、支社、部署を全件取得
			List<User> userData = userService.findAllUser();
			List<Branch> branchesData = branchesService.findAllBranches();
			List<Department> departmentData = departmentService.findAllDepartment();
			// 画面遷移先を指定
			mav.setViewName("/userManagement");
			// データオブジェクトを保管
			mav.addObject("users", userData);
			mav.addObject("branches", branchesData);
			mav.addObject("departments", departmentData);
			mav.addObject("errorMessages", errorMessages);
			mav.setViewName("/userManagement");
			return mav;
		}
		user.setPassword("");
		// 編集するユーザーをセット
		mav.addObject("formModel", user);
		// 画面遷移先を指定
		mav.setViewName("/editUser");
		return mav;
	}

	// ユーザー編集処理
	@PostMapping("/update/{id}")
	public ModelAndView updateUser(@RequestParam(name = "passwordVerification", required = false) String checkPassword,
			@PathVariable Integer id,  @ModelAttribute("formModel") User user) {
		// idを更新するentityにセット
		user.setId(id);
		User loginUser = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		if (StringUtils.isBlank(user.getAccount())) {
			errorMessages.add("アカウント名を入力してください");
		}else if(user.getAccount().matches("[azAZ0*9]") ) {
			errorMessages.add("アカウント名が不正です");
		}else if (user.getAccount().length() > 20 || user.getAccount().length() < 6) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		}else if (userService.checkUniqueUser(user.getAccount()) != null &&
				userService.checkUniqueUser(user.getAccount()).getId() != user.getId()) {
			errorMessages.add("アカウントが重複しています");
		}
		if(!StringUtils.isBlank(user.getPassword())) {
			if (user.getPassword().length() >= 21 || user.getPassword().length() <= 5) {
				errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
			} else if (!user.getPassword().equals(checkPassword)) {
				errorMessages.add("パスワードの確認に失敗しました");
			}
		}
		if (StringUtils.isBlank(user.getName())) {
			errorMessages.add("氏名を入力してください");
		}else if (user.getName().length() > 10) {
			errorMessages.add("氏名は10文字以下で入力してください");
		}else if(user.getName().matches("/^[a-zA-Zぁ-んァ-ンヴー一-龠]*$/") ) {
			errorMessages.add("氏名が不正です");
		}

		if (user.getBranchId() == 0) {
			if (user.getDepartmentId() > 1) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		} else {
			if (user.getDepartmentId() < 2) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if(loginUser.getId() == user.getId()) {
			if(user.getBranchId() != 0) {
				errorMessages.add("自身の部署は変更できません");
			}
			if(user.getDepartmentId() != 0) {
				errorMessages.add("自身の支社は変更できません");
			}
		}

		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			// 編集するユーザーをセット
			mav.addObject("formModel", user);
			// 画面遷移先を指定
			mav.setViewName("/editUser");
			return mav;
		}

		// パスワード暗号化
		String encPassword;
		if (StringUtils.isBlank(user.getPassword())) {
			encPassword = null;
		} else {
			encPassword = CipherUtil.encrypt(user.getPassword());
		}
		user.setPassword(encPassword);
		if (StringUtils.isBlank(user.getPassword()) && StringUtils.isBlank(checkPassword)) {
			// 編集するユーザーを呼び出す
			User eUser = userService.oneUser(id);
			// 元のパスワードで空欄を上書き
			user.setPassword(eUser.getPassword());
		}
		// 更新日時を追加
		Date date = new Date();
		user.setUpdatedDate(date);
		// 編集したユーザー情報で更新
		userService.saveUser(user);
		// 管理画面へリダイレクト
		return new ModelAndView("redirect:/userManagement");
	}


	// ユーザー登録画面
	@GetMapping("/signup")
	public ModelAndView newUser() {
		ModelAndView mav = new ModelAndView();
		// 空のentityを準備
		User user = new User();
		// 画面遷移先を指定
		mav.setViewName("/signup");
		// 準備した空のentityを保管
		mav.addObject("formModel", user);
		return mav;
	}

	// ユーザー登録処理
	@PostMapping("/signup")
	public ModelAndView addUser(@RequestParam(name = "passwordVerification", required = false) String checkPassword,
		@ModelAttribute("formModel") User user) {
		List<String> errorMessages = new ArrayList<String>();

		if (StringUtils.isBlank(user.getAccount())) {
			errorMessages.add("アカウント名を入力してください");
		}else if(user.getAccount().matches("[azAZ0*9]") ) {
			errorMessages.add("アカウント名が不正です");
		}else if (user.getAccount().length() > 20 || user.getAccount().length() < 6) {
			errorMessages.add("アカウント名は6文字以上20文字以下で入力してください");
		}else if (userService.checkUniqueUser(user.getAccount()) != null) {
			errorMessages.add("アカウントが重複しています");
		}
		if (StringUtils.isBlank(user.getPassword())) {
			errorMessages.add("パスワードを入力してください");
		} else if (user.getPassword().length() >= 21 || user.getPassword().length() <= 5) {
			errorMessages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (!user.getPassword().equals(checkPassword)) {
			errorMessages.add("パスワードの確認に失敗しました");
		}
		if (StringUtils.isBlank(user.getName())) {
			errorMessages.add("氏名を入力してください");
		}else if (user.getName().length() > 10) {
			errorMessages.add("氏名は10文字以下で入力してください");
		}else if(user.getName().matches("/^[a-zA-Zぁ-んァ-ンヴー一-龠]*$/") ) {
			errorMessages.add("氏名が不正です");
		}

		if (user.getBranchId() == 0) {
			if (user.getDepartmentId() > 1) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		} else {
			if (user.getDepartmentId() < 2) {
				errorMessages.add("支社と部署の組み合わせが不正です");
			}
		}

		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			// 編集するユーザーをセット
			mav.addObject("formModel", user);
			// 画面遷移先を指定
			mav.setViewName("/signup");
			return mav;
		}
		// パスワード暗号化
		String encPassword;
		if (StringUtils.isBlank(user.getPassword())) {
			encPassword = null;
		} else {
			encPassword = CipherUtil.encrypt(user.getPassword());
		}
		user.setPassword(encPassword);
		// 情報をテーブルに格納
		Date createdDate = new Date();
		Date updatedDate = new Date();
		user.setCreatedDate(createdDate);
		user.setUpdatedDate(updatedDate);
		user.setIsStopped(0);
		userService.saveUser(user);
		// rootへリダイレクト
		return new ModelAndView("redirect:/userManagement");
	}

	// ユーザー復活停止処理
	@PutMapping("/switch/{id}")
	public ModelAndView isStopped(@PathVariable Integer id, @RequestParam("isStopped") int isStopped) {
		User loginUser = (User) session.getAttribute("loginUser");
		User user = new User();
		List<String> errorMessages = new ArrayList<String>();
		user = userService.oneUser(id);
		if(loginUser.getId() == user.getId()) {
			errorMessages.add("自身の稼働状態は変更できません");
		}
		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			List<User> userData = userService.findAllUser();
			List<Branch> branchesData = branchesService.findAllBranches();
			List<Department> departmentData = departmentService.findAllDepartment();
			// 画面遷移先を指定
			mav.setViewName("/userManagement");
			// データオブジェクトを保管
			mav.addObject("users", userData);
			mav.addObject("branches", branchesData);
			mav.addObject("departments", departmentData);
			return mav;
		}
		Date now = new Date();
		user.setUpdatedDate(now);
		if (isStopped == 0) {
			user.setIsStopped(1);
		} else {
			user.setIsStopped(0);
		}
		userService.saveUser(user);
		// 管理画面へリダイレクト
		return new ModelAndView("redirect:/userManagement");
	}

	@GetMapping("/editUser/")
	public ModelAndView editUserError() {
		ModelAndView mav = new ModelAndView();
		List<String> errorMessages = new ArrayList<String>();
		errorMessages.add("不正なパラメータが入力されました");
		mav.addObject("errorMessages", errorMessages);
		// ユーザー、支社、部署を全件取得
		List<User> userData = userService.findAllUser();
		List<Branch> branchesData = branchesService.findAllBranches();
		List<Department> departmentData = departmentService.findAllDepartment();
		// 画面遷移先を指定
		mav.setViewName("/userManagement");
		// データオブジェクトを保管
		mav.addObject("users", userData);
		mav.addObject("branches", branchesData);
		mav.addObject("departments", departmentData);
		return mav;
	}


}
