package com.example.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Message;
import com.example.demo.entity.User;
import com.example.demo.service.BranchesService;
import com.example.demo.service.CommentService;
import com.example.demo.service.DepartmentService;
import com.example.demo.service.MessageService;
import com.example.demo.service.UserService;

@Controller
@SessionAttributes(types = User.class)

public class ContentController {
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	@Autowired
	UserService userService;
	@Autowired
	BranchesService branchesService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	HttpSession session;

	@GetMapping
	public ModelAndView topRedirect() {
		return new ModelAndView("redirect:/sugarashi");
	}

	// トップページ
	@GetMapping("/sugarashi")
	public ModelAndView top(HttpServletRequest request) {
		User loginUser = (User) session.getAttribute("loginUser");
		ModelAndView mav = new ModelAndView();
		Comment comment = new Comment();

		// 投稿を全件取得
		List<Message> textData = messageService.findAllReport();
		// コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		//ユーザーを全件取得
		List<User> userData = userService.findAllUser();

		// 画面遷移先を指定
		mav.setViewName("/top");
		//空のenity用意
		mav.addObject("formModel", comment);
		// 投稿データオブジェクトを保管
		mav.addObject("messages", textData);
		// コメントデータオブジェクトを保管
		mav.addObject("comments", commentData);
		//ユーザー情報を保管
		mav.addObject("users", userData);
		//ログインユーザー情報を保管
		mav.addObject("loginUser", loginUser);

		if (request.getSession().getAttribute("errorMessages") != null) {
			mav.addObject("errorMessages", request.getSession().getAttribute("errorMessages"));
			request.getSession().removeAttribute("errorMessages");
		}

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newText() {
		ModelAndView mav = new ModelAndView();
		// 空のentityを準備
		Message message = new Message();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", message);
		return mav;
	}

	// 新規投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@Validated @ModelAttribute("formModel") Message message, BindingResult result) {
		User loginUser = (User) session.getAttribute("loginUser");

		if (result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
	        List<String> errorList = new ArrayList<String>();

            for (ObjectError error : result.getAllErrors()) {
                errorList.add(error.getDefaultMessage());
            }
            mav.addObject("validationError", errorList);
    		// 画面遷移先を指定
    		mav.setViewName("/new");
    		return mav;
		}
		List<String> errorMessages = new ArrayList<String>();

		if (errorMessages.size() != 0) {
			ModelAndView mav = new ModelAndView();
			mav.addObject("errorMessages", errorMessages);
			// 画面遷移先を指定
			mav.setViewName("/new");
			return mav;
		}

		Date createdDate = new Date();
		Date updatedDate = new Date();
		message.setCreatedDate(createdDate);
		message.setUpdatedDate(updatedDate);
		message.setUserId(loginUser.getId());

		// 投稿をテーブルに格納
		messageService.saveReport(message);
		// トップへリダイレクト
		return new ModelAndView("redirect:/sugarashi");
	}

	//削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		messageService.deleteMessage(id);
		return new ModelAndView("redirect:/sugarashi");
	}

	// コメント処理
	@PostMapping("/comment/{id}")
	public ModelAndView addComment(@Validated @ModelAttribute("formModel") Comment comment, BindingResult result, @PathVariable Integer id) {
		User loginUser = (User) session.getAttribute("loginUser");
		ModelAndView mav = new ModelAndView();

		if (result.hasErrors()) {
	        List<String> errorMessages = new ArrayList<String>();

            for (ObjectError error : result.getAllErrors()) {
                errorMessages.add(error.getDefaultMessage());
            }
            mav.addObject("errorMessages", errorMessages);

    		// 投稿を全件取得
    		List<Message> textData = messageService.findAllReport();
    		// コメントを全件取得
    		List<Comment> commentData = commentService.findAllComment();
    		//ユーザーを全件取得
    		List<User> userData = userService.findAllUser();

    		//空のenity用意
    		mav.addObject("formModel", comment);
    		// 投稿データオブジェクトを保管
    		mav.addObject("messages", textData);
    		// コメントデータオブジェクトを保管
    		mav.addObject("comments", commentData);
    		//ユーザー情報を保管
    		mav.addObject("users", userData);
    		//ログインユーザー情報を保管
    		mav.addObject("loginUser", loginUser);
    		// 画面遷移先を指定
    		mav.setViewName("/top");
    		return mav;
		}

			Date createdDate = new Date();
			Date updatedDate = new Date();

			comment.setCreatedDate(createdDate);
			comment.setUpdatedDate(updatedDate);
			comment.setMessageId(id);
			comment.setUserId(loginUser.getId());

			// 投稿をテーブルに格納
			commentService.saveComment(comment);
			// トップへリダイレクト
			return new ModelAndView("redirect:/sugarashi");
		}

	//コメント削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	// 表示絞り込み
	@GetMapping("/search")
	public ModelAndView betweenTop(@RequestParam("start") String start, @RequestParam("end") String end,
			@RequestParam(name="category" ,required = false) String category) {
		ModelAndView mav = new ModelAndView();
		Comment comment = new Comment();
		List<Message> messageBetweenData = messageService.findBetweenMessage(start, end);
		List<Message> messageCategoryData = messageService.findCategoryMessage(category);
		List<Comment> commentData = commentService.findAllComment();
		List<User> userData = userService.findAllUser();
		List<Message> messageData = new ArrayList<Message>();
		// すべて未入力
		if(category == "" && start == "" && end == "") {
			messageData = messageService.findAllReport();
		// カテゴリが入力で日付は片方のみあるいは両方入力
		}else if(category != "" && (start != "" || end != "")) {
	        for(Message a : messageBetweenData){
	            for(Message b : messageCategoryData){
	                if(a.getId()==b.getId()){
	                    messageData.add(a);
	                    break;
	                }
	            }
	        }
	    // カテゴリが未入力で日付は片方のみあるいは両方入力
		}else if(category == "" && (start != "" || end != "")){
			messageData = messageBetweenData;
		// カテゴリが入力で日付は両方未入力
		}else if(category != "" && start == "" && end == "") {
			messageData = messageCategoryData;
		}

		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("messages", messageData);
		mav.addObject("comments", commentData);
		//日付とカテゴリの保管
		mav.addObject("start", start);
		mav.addObject("end", end);
		mav.addObject("category", category);
		//ユーザー情報を保管
		mav.addObject("users", userData);
		//空のenity用意
		mav.addObject("formModel", comment);
		return mav;
	}


}