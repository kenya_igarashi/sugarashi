package com.example.demo.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.demo.entity.User;

@WebFilter(urlPatterns = { "/userManagement", "/signup", "/editUser" })
public class AdminFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		// 人事部のid
		int humanResourceDepartment = 0;
		// 本社のid
		int headOffice = 0;

		User adminCheck = (User) session.getAttribute("loginUser");

		if (adminCheck.getDepartmentId() == humanResourceDepartment && adminCheck.getBranchId() == headOffice) {
			chain.doFilter(request, response);
		} else {
			session = ((HttpServletRequest) request).getSession(true);
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("/sugarashi");
			return;
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
