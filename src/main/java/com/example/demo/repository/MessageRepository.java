package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	List<Message> findByCreatedDateBetweenOrderByUpdatedDateDesc(Date startDate, Date endDate);
	List<Message> findByCategoryLike(String category);
	List<Message> findByOrderByCreatedDateDesc();

}