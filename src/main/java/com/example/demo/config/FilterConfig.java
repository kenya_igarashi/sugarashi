package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.AdminFilter;
import com.example.demo.filter.LoginFilter;

@Configuration
public class FilterConfig {

	// 管理者フィルタ
	@Bean
	public FilterRegistrationBean AdminFilter() {
		FilterRegistrationBean bean = new FilterRegistrationBean(new AdminFilter());
		bean.addUrlPatterns("/userManagement", "/signup", "/editUser/*");
		bean.setOrder(2);
		return bean;
	}

	// ログインフィルタ
	@Bean
	public FilterRegistrationBean LoginFilter() {
		FilterRegistrationBean bean = new FilterRegistrationBean(new LoginFilter());
		bean.addUrlPatterns(
				"/admin/*", "/", "/sugarashi", "/userManagement", "/signup", "/editUser/*",
				"/new", "/add", "/top", "/comment/*");
		bean.setOrder(1);
		return bean;
	}

}