package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessageService {
	@Autowired
	MessageRepository messageRepository;


	// レコード全件取得
	public List<Message> findAllReport() {
        List<Message> message = messageRepository.findByOrderByCreatedDateDesc();

		return message;
	}
	//新規投稿
	public void saveReport(Message message) {
		messageRepository.save(message);
	}
	//投稿削除
	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id);
	}

	// 範囲取得
	public List<Message> findBetweenMessage(String start, String end) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		Date startDate = null;
		Date endDate = null;
		// 投稿取得ここに絞り込みデータ投下
		if (start.isEmpty()) {
			start += "2020/01/01 00:00:00";
		}else {
			start += " 00:00:00";
			start = start.replace("-", "/");
		}
		if (end.isEmpty()) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			end = format.format(timestamp);
		}else {
			end += " 23:59:59";
			end = end.replace("-", "/");
		}
		try {
			startDate = format.parse(start);
			endDate = format.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return messageRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(startDate, endDate);
	}

	// カテゴリ取得
	public List<Message> findCategoryMessage(String category) {
		if(StringUtils.isBlank(category)) {
			category = null;
		}
		return messageRepository.findByCategoryLike("%" + category + "%");
	}


}
