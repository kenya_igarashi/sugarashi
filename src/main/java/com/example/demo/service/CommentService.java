package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	//新規投稿
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	// レコード全件取得
	public List<Comment> findAllComment() {
	return commentRepository.findByOrderByCreatedDateAsc();

	}

	//編集する投稿呼び出し
	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	//投稿削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}
}
