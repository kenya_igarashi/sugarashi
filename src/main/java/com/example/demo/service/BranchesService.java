package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Branch;
import com.example.demo.repository.BranchesRepository;

@Service
public class BranchesService {
	@Autowired
	BranchesRepository branchesRepository;

	// ユーザー全件取得
	public List<Branch> findAllBranches() {
		return branchesRepository.findAll();
	}
}
