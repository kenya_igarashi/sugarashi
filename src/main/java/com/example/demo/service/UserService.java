package com.example.demo.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.utils.CipherUtil;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	//ログイン時のセレクト
	public User loginUser(User loginUser){
		String account = loginUser.getAccount();

		String encPassword;
		if (StringUtils.isBlank(loginUser.getPassword())) {
			encPassword = null;
		} else {
			encPassword = CipherUtil.encrypt(loginUser.getPassword());
		}

		List<User> user = userRepository.findByAccountAndPassword(account, encPassword);
		if (user.isEmpty()){
			return null;
		}
		return user.get(0);
	}

	// ユーザー全件取得
	public List<User> findAllUser() {
		return userRepository.findByOrderByCreatedDateAsc();
	}

	// ユーザー新規登録、編集、停止
	public void saveUser(User user) {
	userRepository.save(user);
	}

	// ユーザー単件取得
	public User oneUser(Integer id) {
		User user = (User)userRepository.findById(id).orElse(null);
		return user;
	}

	// ユーザー登録重複確認
	public User checkUniqueUser(String account) {
		List<User> user = userRepository.findByAccount(account);
		if (user.isEmpty()){
			return null;
		}
		return user.get(0);
	}


}
